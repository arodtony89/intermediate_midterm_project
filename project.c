#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "ppm_io.h"
#include <string.h>
#include "image_functions.h"
#include "image_functions2.h"


int main(int argc, char *argv[]){
	// check if there are even enough arguments to check
	if (argc < 4){
		printf("Failed to supply input and/or output filename. Try again\n");
		return 1;
	}
	// check if fourth argument is proper operation name
	char str1[] = "swap";
	char str2[] = "grayscale";
	char str3[] = "contrast";
	char str4[] = "zoom_in";
	char str5[] = "zoom_out";
	char str6[] = "occlude";
	char str7[] = "blur";
	if (strcmp(argv[3],str1) != 0 && strcmp(argv[3],str2) != 0 && strcmp(argv[3],str3) != 0 && strcmp(argv[3],str4) != 0 && strcmp(argv[3], str5) != 0 && strcmp(argv[3],str6) != 0 && strcmp(argv[3],str7) != 0){
		printf("No operation name was specified/invalid operation specified. Try again\n");
		return 4;
	}

	// check if input file can be opened
	FILE *input = fopen(argv[1], "rb");
	if (input == NULL){
		printf("Input file could not be opened. Try again\n");
		return 2;
	}

	// read in data to file and check if properly formatted PPM file
	Image * orig= read_ppm(input);
	if (orig == NULL){
		printf("Not properly formatted PPM file or reading input failed. Try again\n");
		return 3;
	}
	fclose(input);
	// code operation specific errors and such
	// This calls the swap function
	// make pointer to new image
	
	if (strcmp(argv[3], str1) == 0 && argc == 4){
		swap(orig);
	}
	
	// This calls the greyscale function
	else if (strcmp(argv[3], str2) == 0 && argc == 4){
		grayscale(orig);
	}
	// This calls the contrast function
	else if (strcmp(argv[3], str3) == 0 && argc == 5){
		double contrast_val = 0.0;
		int c = 2;
		c = sscanf(argv[4], "%lf", &contrast_val);
		//this will tell us whether the wrong kind of input was specified
		if (c != 1){
			printf("Incorrect kind of argument specified. Please try again\n");
			return 5;
		}
		contrast(orig, contrast_val);
	}

	
	// This calls the zoom_in function
	else if (strcmp(argv[3], str4) == 0 && argc == 4){
		zoom_in(orig);
	}
	// This calls in the zoom_out function
	else if (strcmp(argv[3], str5) == 0 && argc == 4){
		Image* fresh = malloc(sizeof(Image));
		fresh->data = malloc(sizeof(Pixel)*(orig->cols)*(orig->rows)/4);
		fresh->rows = (orig->rows)/2;
		fresh->cols = (orig->cols)/2;
		zoom_out(orig, fresh);
		free(orig->data);
		free(orig);
		orig = fresh;

	}
	// This calls in the occlude function
	else if (strcmp(argv[3], str6) == 0 && argc == 8){
		int left = 0;
		int top = 0;
		int bottom = 0;
		int right = 0;
		int c = 3;
		c = sscanf(argv[4], "%d", &left);
		if (c != 1){
			printf("Incorrect kind of argument specified. Please try again\n");
			return 5;
		}
		c = 2;
		c = sscanf(argv[5], "%d", &top);
		if (c != 1){
			printf("Incorrect kind of argument specified. Please try again\n");
			return 5;
		}
		c = 2;
		c = sscanf(argv[6], "%d", &right);
		if (c != 1){
			printf("Incorrect kind of argument specified. Please try again\n");
			return 5;
		}
		c = sscanf(argv[7], "%d", &bottom);
		if (c != 1){
			printf("Incorrect kind of argument specified. Please try again\n");
			return 5;
		}

		//here we check if the bounds fit in the image
		if(top < 0 || left < 0 || right >= orig->cols || bottom >= orig->rows){
		  printf("Arguments for occlude operation were out of range for the given input image\n");
		  return 6;
		}
		
		occlude(orig, left, top, right, bottom);
	}
	// This is for blur function	
	else if (strcmp(argv[3], str7) == 0 && argc == 5){
		double sigma = 0;
		int c = 3;
		c = sscanf(argv[4], "%lf", &sigma);
		if (c != 1){
			printf("Incorrect kind of argument specified. Please try again\n");
			return 5;
		}
		//need to include call for function
		int size = 0;
		double * gaussian_filter = gaussian_matrix(sigma, &size);
		blur(orig, sigma, gaussian_filter, size);
		free(gaussian_filter);
	}

	
	//This tells us there is an error in the number of arguments
	else{
		printf("Incorrect number of arguments specified. Please try again\n");
		return 5;
	}
	// check if orig is null
	if (orig == NULL){
		printf("The target array wasn't written to\n");
		return 8;
	}

	// write data to file
	FILE *output = fopen(argv[2], "wb");
	if (output == NULL){
		printf("Specified output file couldn't be opened for writing. Try again\n");
		return 7;
	}
	//int num_pix_written
	write_ppm(output,orig);
	fclose(output);
	//free the memory 
	free(orig->data);
	free(orig);
	return 0;
}


