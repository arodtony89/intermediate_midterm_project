#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "image_functions.h"
#include "ppm_io.h"

Image * copy(const Image *im){
	// allocate memeory to new pointer
	Image * new_copy = malloc(sizeof(Image));
	new_copy->rows = im->rows;
	new_copy->cols = im->cols;
	// allocate memory to data array
	new_copy->data = malloc(sizeof(Pixel) * (im->rows) * (im->cols));
	// store rgb values into array
	for (int a = 0; a < ((im->rows)*(im->cols)); a++){
		new_copy->data[a].r = im->data[a].r;
		new_copy->data[a].g = im->data[a].g;
		new_copy->data[a].b = im->data[a].b;
	}
	return new_copy;
}

void swap(Image *im){
	// create variables to store num of cols and rows
	int col_num = im->cols;
	int row_num = im->rows;
	// create variables to store current variable values
	unsigned char r_temp;
	unsigned char g_temp;
	unsigned char b_temp;
	// make pointer to data array
	Pixel *curr;
	curr = im->data;
	//iterate over every pixel in array
	for (int i = 0; i < (col_num * row_num); i++){
		// store current state of colors
		r_temp = (curr+i)->r;
		g_temp = (curr+i)->g;
		b_temp = (curr+i)->b;
		// switch the color;
		(curr+i)->r = g_temp;
		(curr+i)->g = b_temp;
		(curr+i)->b = r_temp;
	}
}

void grayscale(Image *im){
	// iterate over every pixel in array
	int col_num = im->cols;
	int row_num = im->rows;
	double r_temp;
	double g_temp;
	double b_temp;
	double intensity; 
	// create pointer to data array
	Pixel *curr;
	curr = im->data;
	for (int i = 0; i < (col_num * row_num); i++){
			// save the value of the current pixel
			r_temp = (double)((curr+i)->r);
			g_temp = (double)((curr+i)->g);
			b_temp = (double)((curr+i)->b);
			// calculate the intensity
			intensity = (0.30*r_temp) + (0.59*g_temp) + (0.11*b_temp);
			// change up the value of the pixel
			(curr+i)->r = (unsigned char)(intensity);
			(curr+i)->g = (unsigned char)(intensity);
			(curr+i)->b = (unsigned char)(intensity);
	}
}

void contrast(Image *im, double adj){
	// create variables to store rows, cols, and temporary and adjusted values
	int col_num = im->cols;
	int row_num = im->rows;
	double r_temp;
	double g_temp;
	double b_temp;
	double r_adj;
	double g_adj;
	double b_adj;
	// make pointer to data array
	Pixel *curr;
	curr = im->data;
	for (int i = 0; i < (row_num * col_num); i++){
			r_temp = (double)((curr+i)->r);
			g_temp = (double)((curr+i)->g);
			b_temp = (double)((curr+i)->b);
			// transform unsigned representation to converted adjustment
			r_adj = (-0.5*adj) + (adj)*(1.0/255.0) * (r_temp);
			g_adj = (-0.5*adj) + (adj)*(1.0/255.0) * (g_temp);
			b_adj = (-0.5*adj) + (adj)*(1.0/255.0) * (b_temp);
			// check if each adj value is beyond range
			if (r_adj > 0.5){
				r_adj = 0.5;
			}
			else if (r_adj < -0.5){
				r_adj = -0.5;
			}
			if (g_adj > 0.5){
				g_adj = 0.5;
			}
			else if (g_adj < -0.5){
				g_adj = -0.5;
			}
			if (b_adj > 0.5){
				b_adj = 0.5;
			}
			else if (b_adj < -0.5){
				b_adj = -0.5;
			}
			// store converted value into im matrix
			(curr+i)->r = (unsigned char)(255*(r_adj + 0.5));
			(curr+i)->g = (unsigned char)(255*(g_adj + 0.5));
			(curr+i)->b = (unsigned char)(255*(b_adj + 0.5));
	}
}
double * gaussian_matrix(double sigma, int * gaussian_row_num){
	// create initial number of rows and create pointer to filter
	int init_rows = (int) ceil(10*sigma);
	int total_rows = 0;
	double * g_matrix;
	// allocate the memory depending on whether init_row is even or odd
	if ((init_rows % 2) == 0){
		g_matrix = malloc(sizeof(double) * (init_rows + 1) * (init_rows + 1));
		total_rows = init_rows + 1;
	}
	else{
		g_matrix = malloc(sizeof(double) * (init_rows) * (init_rows));
		total_rows = init_rows;
	}
	// define a constant pi
	const double pi = 4.0 * atan(1.0);
	// define constant term of gaussian filter
	double g_const = 1.0/(2.0 * sigma * sigma * pi);
	int curr_row = 0;
	int curr_col = 0;
	double sqr_sum = 0.0;
	int dx = 0;
	int dy = 0;
	// iterate through each element and add value
	for (int i = 0; i < total_rows; i++){
		dx = (int)(floor(total_rows / 2)) + i + 1 - total_rows;
		for (int j = 0; j < total_rows; j++){
 			dy = dx - i + j;
			sqr_sum = (dx * dx) + (dy * dy);
			*(g_matrix + (i*total_rows) + j) = g_const * exp(-1.0 * sqr_sum / (2.0 * sigma * sigma));
		}
	}
	// return row num and gaussian matrix
	*gaussian_row_num = total_rows;
	return g_matrix;
}

void blur(Image *im, double sig, double *m, int size){
	// define constants
	int dist = size/2;
	int cols = im->cols;
	int rows = im->rows;
	// convolve each pixel
	for (int i = 0; i < rows; i++){
		for (int j = 0; j < cols; j++){
			convolve(dist, i, j, im->data, size, m, rows, cols);
		}
	}
}

void convolve(int dist, int i, int j, Pixel* data, int N, double * m, int rows, int cols){
	//define the bounds for the gaussian matrix
	int top = 0;
	int bottom = N-1;
	int left = 0; 
	int right = N-1;
	ignore(&top, &bottom, &left, &right, i, j, dist, N, rows, cols);

	// find the weights of neighbor pixels
	Pixel * cur;
	double r_sum = 0;
	double g_sum = 0;
	double b_sum = 0;
	double total = 0.0;
	// sum the linear combination of weights of pixels in filter
	for (int c = top; c < bottom + 1; c++){
		for (int d = left; d < right + 1; d++){
			cur = &data[cols * (c - dist + i) + (d - dist + j)];
			r_sum += cur->r * (*(m + c*N + d));
			g_sum += cur->g * (*(m + c*N + d));
			b_sum += cur->b * (*(m + c*N + d));
			total += *(m + c*N + d);
		}
	}

	cur = &data[cols*i + j];
	// set value of new pixel
	double new_r = r_sum / total;
	double new_g = g_sum / total;
	double new_b = b_sum / total;
	// check if in range
	if (new_r < 0)
		new_r = 0;
	else if (new_r > 255)
		new_r = 255;
	if (new_g < 0)
		new_g = 0;
	else if (new_g > 255)
		new_g = 255;
	if (new_b < 0)
		new_b = 0;
	else if (new_b > 255)
		new_b = 255;
	cur->r = (unsigned char) new_r;
	cur->g = (unsigned char) new_g;
	cur->b = (unsigned char) new_b;
}

void ignore(int* top, int* bottom, int* left, int* right, int i, int j, int dist, int N, int rows, int cols){
	//sets up bounds for the convolution
	if (j < cols/2){
		*left = dist - j;
		if (*left < 0)
			*left = 0;
	}
	else if (j > cols/2){
		*right = N - 1 - (dist - (cols - 1 - j));
		if (*right > N - 1)
			*right = N -1;
	}
	if (i < rows/2){
		*top = dist - i;
		if (*top < 0)
			*top = 0;
	}
	else if (i > rows/2){
		*bottom = N - 1 - (dist - (rows -1 - i));
		if (*bottom > N-1)
			*bottom = N-1;
	}
}
