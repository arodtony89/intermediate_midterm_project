#ifndef IMAGE_FUNCTIONS2_H
#define IMAGE_FUNCTIONS2_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "ppm_io.h"

void zoom_in(Image *im);
void zoom_out(Image *im, Image *fresh);
void occlude(Image *im,int x1,int x2,int y1, int y2);

#endif
