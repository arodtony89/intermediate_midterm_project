CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -g

project: project.o ppm_io.o image_functions.o image_functions2.o
	$(CC) project.o ppm_io.o image_functions.o image_functions2.o -o project -lm -g

read_write_test: experiment_fileio.o ppm_io.o
	$(CC) experiment_fileio.o ppm_io.o -o read_write_test -lm

demo: demo_ppm.o ppm_io.o
	$(CC) demo_ppm.o ppm_io.o -o demo -lm

experiment_fileio.o: experiment_fileio.c ppm_io.h
	$(CC) $(CFLAGS) -c experiment_fileio.c -lm

demo_ppm.o: demo_ppm.c ppm_io.h
	$(CC) $(CFLAGS) -c demo_ppm.c -lm

ppm_io.o: ppm_io.c ppm_io.h
	$(CC) $(CFLAGS) -c ppm_io.c -lm

image_functions.o: image_functions.c image_functions.h ppm_io.h
	$(CC) $(CFLAG) -c image_functions.c -lm

image_functions2.o: image_functions2.c image_functions2.h ppm_io.h
	$(CC) $(CFLAG) -c image_functions2.c -lm

clean:
	rm -f *.o demo read_write_test
