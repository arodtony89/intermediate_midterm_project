#ifndef IMAGE_FUNCTIONS_H
#define IMAGE_FUNCTIONS_H	

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "ppm_io.h"

Image * copy(const Image *im);
void swap(Image *im);
void grayscale(Image *im);
void contrast(Image *im, double adj);
double * gaussian_matrix(double sigma, int * gaussian_row_value);
void blur(Image * im, double sig, double *m, int size);
void convolve(int dist, int i, int j, Pixel* data, int N, double * m, int rows, int cols);
void ignore(int* top, int* bottom, int* left, int*right, int i, int j, int dist, int N, int rows, int cols);
#endif
