// ppm_io.c
// 601.220, Fall 2018
// Starter code for midterm project - feel free to edit/add to this file

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ppm_io.h"



/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *input){
  // check that fp is not NULL
  	if (input == NULL)
		return NULL;
	char P6[3];
	char line[1100];
	int nums[3];
	int count = 0;
	char pound = 0;
	fgets(line, 1100, input);
	// see if file in proper format
	if (sscanf(line, "%s", P6) == 1 && strcmp(P6, "P6") == 0){
	       if (sscanf(line, "%d", &nums[count]) == 1){
		       count++;
		       if (sscanf(line, "%*d %d", &nums[count]) == 1){
				count++;
		 		if (sscanf(line, "%*d %*d %d", &nums[count]) == 1){
					count ++;
				}
		       }
	       }
	}
	if (strcmp(P6, "P6") != 0){
		return NULL;
	}
	while (count < 3){
		fgets(line, 1100, input);
		if (sscanf(line, "%c", &pound) == 1 && pound == '#'){
			pound = 0;
		}
		else{
			if(sscanf(line, "%d", &nums[count]) == 1){
				count++;
				if(sscanf(line, "%*d %d", &nums[count]) == 1){
					count++;
					if (sscanf(line, "%*d %*d %d", &nums[count]) == 1){
						count++;
					}
				}
			}
		}
	}
	// generate an image struct and store information in it
	Image * im = malloc(sizeof(Image));
	im->rows = nums[1];
	im->cols = nums[0];
	
	Pixel *pix = malloc(sizeof(Pixel) * nums[0] * nums[1]);
	fread(pix, sizeof(Pixel), nums[0] * nums[1], input);
	im->data = pix;
	return im;
}



/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im){

  // check that fp is not NULL 
  assert(fp);
  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), (im->rows * im->cols), fp);

  if (num_pixels_written != (im->rows * im->cols)) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}

