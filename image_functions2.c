#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "image_functions.h"
#include "ppm_io.h"

void zoom_in(Image *im){
  int orig_cols = im->cols;
  int orig_rows = im->rows;
  im->cols = orig_cols * 2;
  im->rows = orig_rows * 2;
  im->data = realloc(im->data,sizeof(Pixel)*(im->cols)*(im->rows));
  Pixel *curr;
  curr = im->data;
  for(int i = orig_rows * orig_cols;i>=0;i--){
    unsigned char r = (curr+i)->r;
    unsigned char b = (curr+i)->b;
    unsigned char g = (curr+i)->g;
    int top_right = 2*((i % orig_cols) + orig_cols * (i/orig_cols)*2);
    Pixel * target = (curr+top_right);
    target->r = r;
    target->g = g;
    target->b = b;
    target = (curr+ top_right+ (im->cols));
    target->r = r;
    target->g = g;
    target->b = b;

    target = (curr + top_right + 1);
    target->r = r;
    target->g = g;
    target->b = b;

    target = (curr + top_right + 1 + (im->cols));
    target->r = r;
    target->g = g;
    target->b = b;
  }
}

void zoom_out(Image *im, Image *new){
  int col_num = im->cols;
  int row_num = im->rows;
  Pixel * curr;
  curr = im->data;
  Pixel * paint_to;
  paint_to = new->data;
  for(int x = 0; x < col_num; x+=2){
    for(int y = 0; y<row_num; y+=2){
      int rtot=0;
      int gtot=0;
      int btot=0;
      
      int i = x + y*col_num;
      rtot += (curr + i)->r;
      gtot += (curr + i)->g;
      btot += (curr + i)->b;

      i = x + 1 + y*col_num;
      rtot += (curr + i)->r;
      gtot += (curr + i)->g;
      btot += (curr + i)->b;

      i = x + (y+1)*col_num;
      rtot += (curr+i)->r;
      gtot += (curr+i)->g;
      btot += (curr + i)->b;

      i = x + 1 + (y+1)*col_num;
      rtot += (curr + i)->r;
      gtot += (curr + i)->g;
      btot += (curr + i)->b;

      unsigned char rav = rtot/4;
      unsigned char gav = gtot/4;
      unsigned char bav = btot/4;

      (paint_to + x/2 + col_num*y/4)->r = rav;
      (paint_to+ x/2 + col_num*y/4)->b = bav;
      (paint_to + x/2 + col_num*y/4)->g = gav;
      
    }
  }
}

//for occlude '1' is the topleft and '2' is the bottomright
void occlude(Image *im,int x1,int y1,int x2, int y2){

  int col_num = im->cols;
  int row_num = im->rows;
  Pixel * curr;
  curr = im->data;
  for(int x = x1; x < x2; x++){
    for(int y = y1; y < y2; y++){
      (curr + x + col_num*y)->r = (unsigned char) 0;
      (curr + x + col_num*y)->g = (unsigned char) 0;
      (curr + x + col_num*y)->b = (unsigned char) 0;
    }
  }
}
